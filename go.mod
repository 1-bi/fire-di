module gitlab.com/1-bi/fire-di

require (
	gitlab.com/1-bi/log-api v0.0.3
	go.uber.org/atomic v1.3.2
	go.uber.org/dig v1.6.0
	go.uber.org/multierr v1.1.0
	go.uber.org/zap v1.9.1
)
